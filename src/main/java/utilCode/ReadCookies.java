package utilCode;

import java.util.Set;
//import javax.servlet.http.Cookie;
//import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ReadCookies {

    WebDriver driver;

    @BeforeTest
    public void beforeStart() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\abathini\\Selenium\\DRIVERS\\chromedriver.exe");

        // WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
    }

    @AfterTest
    public void afterTest() {
        driver.close();
    }

    @Test
    public void testCase() {
        driver.get("https://www.amazon.in/");
        driver.findElement(By.xpath("//a[@id='nav-cart']")).click();
        driver.manage().getCookies();
        Set<org.openqa.selenium.Cookie> totalCookies = driver.manage()
                .getCookies();

        System.out.println("Total Number Of cookies : " + totalCookies.size());

        for (org.openqa.selenium.Cookie currentCookie : totalCookies) {
            System.out.println(String.format("%s ---> %s ---> %s",
                    "Domain Name : " + currentCookie.getDomain(),
                    "Cookie Name : " + currentCookie.getName(),
                    "Cookie Value : " + currentCookie.getValue()));


        }
    }
}
