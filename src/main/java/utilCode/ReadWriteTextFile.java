package utilCode;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadWriteTextFile {

    public static void main(String[] args) throws IOException {

        File f = new File("D:\\backup\\DataDrivenFiles\\FileRW.txt");
        f.createNewFile();

        // Writing
        FileWriter w = new FileWriter("D:\\backup\\DataDrivenFiles\\FileRW.txt");
        BufferedWriter bfw = new BufferedWriter(w);
        bfw.write("Hello!! I'm writing in this file");
        bfw.newLine();
        bfw.write("This is a new line");
        bfw.flush();

        // Reading
        FileReader r = new FileReader("D:\\backup\\DataDrivenFiles\\FileRW.txt");
        BufferedReader bfr = new BufferedReader(r);

        System.out.println(bfr.readLine());
        System.out.println(bfr.readLine());

    }
}
