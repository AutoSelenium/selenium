package utilCode;

import javax.json.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ReadWriteJSON {

    public static void main(String[] args) {
        writeJSON();
        //readJSON();
    }

    public static void writeJSON() {
        JsonObject value = Json
                .createObjectBuilder()
                .add("Name", "Amrutha")
                .add("Organization", "TGS")
                .add("Department", "CT")
                .add("TestSteps",
                        Json.createArrayBuilder()
                                .add(Json.createObjectBuilder().add("Id", 1)
                                        .add("Step", "Open TGS login page"))
                                .add(Json.createObjectBuilder().add("Id", 2)
                                        .add("Step", "Enter user name"))
                                .add(Json.createObjectBuilder().add("Id", 3)
                                        .add("Step", "Enter user password"))
                                .add(Json.createObjectBuilder().add("Id", 4)
                                        .add("Step", "Click on Login button")))
                // .add("Verification", "Logout link should display")
                // .add("Note", "")
                .build();

        System.out.println(value.toString());
        JsonWriter writer = null;
        try {
            writer = Json
                    .createWriter(new FileOutputStream(
                            new File(
                                    "D:\\Java_Workspace\\Selenium\\src\\test\\java\\com\\tek\\dataDrivenFiles\\RW_JSON.json")));
            writer.writeObject(value);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void readJSON() {
    }

    public static void ReadJSON() throws IOException {

        // define Input Stream for the Json file
        InputStream inputStream = null;
        // define Json Reader
        javax.json.JsonReader jsonReader = null;
        // define Json Object
        JsonObject jsonObject = null;
        try {
            inputStream = new FileInputStream(
                    "D:\\Java_Workspace\\Selenium\\src\\test\\java\\com\\tek\\dataDrivenFiles\\RW_JSON.json");
            jsonReader = Json.createReader(inputStream);
            jsonObject = jsonReader.readObject();
            System.out.println(jsonObject.toString());
            System.out.println(jsonObject.get("Name"));
            JsonArray testSteps = jsonObject.getJsonArray("TestSteps");
            int numberOfSteps = testSteps.size();
            for (int i = 0; i < numberOfSteps; i++) {
                JsonObject eachStep = testSteps.getJsonObject(i);
                System.out.println("Id  " + eachStep.get("Id") + " Step:  "
                        + eachStep.get("Step"));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
            jsonReader.close();
        }
    }
}
