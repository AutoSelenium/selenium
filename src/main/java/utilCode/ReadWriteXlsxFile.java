package utilCode;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ReadWriteXlsxFile {

    public static void main(String[] args) throws Exception {

        // Get the excel file and create an input stream for excel
        FileInputStream fis = new FileInputStream(
                "D:\\Java_Workspace\\Selenium\\src\\test\\java\\DataDrivenFiles\\RW_XlsxFile.xlsx");

        // load the input stream to a workbook object
        // Use XSSF for (.xlsx) excel file and HSSF for (.xls) excel file
        XSSFWorkbook wb = new XSSFWorkbook(fis);

        // get the sheet from the workbook by index
        XSSFSheet sheet = wb.getSheet("Sheet1");

        // Count the total number of rows present in the sheet
        int rowcount = sheet.getLastRowNum();
        System.out.println("Total number of rows present in the sheet : "
                + rowcount);

        // get column count present in the sheet
        int colcount = sheet.getRow(1).getLastCellNum();
        System.out.println("Total number of columns present in the sheet : "
                + colcount);

        // get the data from sheet by iterating through cells
        // by using for loop
        XSSFCell cell, cell2;
        String celltext = "", celltext2 ="";
        for (int i = 1; i <= rowcount; i++) {
            cell = sheet.getRow(i).getCell(1);


            // Get celltype values
            if (cell.getCellType() == CellType.STRING) {
                celltext = cell.getStringCellValue();
                System.out.println(celltext);

            } else if (cell.getCellType() == CellType.NUMERIC) {
                celltext = String.valueOf(cell.getNumericCellValue());
                System.out.println(celltext);
            } else if (cell.getCellType() == CellType.BLANK) {
                celltext = "";
            }

            // Check the age and set the Cell value into excel
            cell2 = sheet.getRow(i).getCell(2);
            if (Double.parseDouble(celltext) >= 18) {
                if (cell2.getCellType() == CellType.STRING) {
                    celltext2 = "Major";
                    cell2.setCellValue(celltext2);
                }
            } else {

                sheet.getRow(i).getCell(2).setCellValue("Minor");
            }

        }// End of for loop

        // close the file input stream
        fis.close();

        // Open an excel to write the data into workbook
        FileOutputStream fos = new FileOutputStream(
                "D:\\Java_Workspace\\Selenium\\src\\test\\java\\DataDrivenFiles\\RW_XlsxFile.xlsx ");

        // Write into workbook
        wb.write(fos);

        // close fileoutstream
        fos.close();
    }

}