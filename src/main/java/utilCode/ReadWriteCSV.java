package utilCode;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;


public class ReadWriteCSV {

    public static void main(String[] args) throws IOException {
        writeCSV();
        readCSV();
    }

    public static void writeCSV() {
        String csvOutputFile = "C:\\Users\\abathini\\IdeaProjects\\SeleniumPractice\\src\\main\\java\\utilFiles\\RW_CSV.csv";
        try {
            CsvWriter testcases = new CsvWriter(new FileWriter(csvOutputFile,
                    true), ',');
            {
                testcases.write("Name");
                testcases.write("Organization");
                testcases.write("Employee ID");
                testcases.write("Designation");
                testcases.write("Work Location");
                testcases.write("Address");
                testcases.endRecord();
            }
            testcases.write("Amrutha");
            testcases.write("TGS");
            testcases.write("3288");
            testcases.write("Tester");
            testcases.write("Hyderabad");
            testcases.write("Cyber Gateway");
            testcases.endRecord();
            testcases.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readCSV() {
        try {

            CsvReader testcases = new CsvReader(
                    "D:\\Java_Workspace\\Selenium\\src\\test\\java\\com\\tek\\dataDrivenFiles\\RW_CSV.csv");

            testcases.readHeaders();
            int numberOfHeaders = testcases.getHeaderCount();
            for (int i = 0; i < numberOfHeaders; i++) {
                System.out.print(testcases.getHeader(i) + ",  ");
            }
            System.out.println();
            while (testcases.readRecord()) {
                String Name = testcases.get("Name");
                String Organization = testcases.get("Organization");
                String EmployeeID = testcases.get("Employee ID");
                String Designation = testcases.get("Designation");
                String WorkLocation = testcases.get("Work Location");
                String Address = testcases.get("Address");
                System.out.println(Name + ",   " + Organization + ",   "
                        + EmployeeID + ",  " + Designation + ",    "
                        + WorkLocation + ",   " + Address);
            }

            testcases.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



