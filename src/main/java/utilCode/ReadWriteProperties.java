package utilCode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadWriteProperties {

    public static void main(String[] args) throws IOException {

        writeFile();
        readFile();
    }

    private static void writeFile() {
        Properties prop = new Properties();
        try {
            prop.setProperty("Name", "Amrutha");
            prop.setProperty("Organization", "TGS");
            prop.setProperty("Location", "Hyderabad");
            prop.setProperty("Department", "CT");

            File file = new File(
                    "C:\\Users\\abathini\\IdeaProjects\\SeleniumPractice\\src\\main\\java\\utilFiles\\ReadWriteProperties");
            prop.store(new FileOutputStream(file), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void readFile() throws IOException {
        Properties prop = new Properties();
        FileInputStream ip = new FileInputStream(
                "C:\\Users\\abathini\\IdeaProjects\\SeleniumPractice\\src\\main\\java\\utilFiles\\ReadWriteProperties");
        prop.load(ip);

        System.out.println(prop.getProperty("Name"));
        System.out.println(prop.getProperty("Organization"));
        System.out.println(prop.getProperty("Location"));
        System.out.println(prop.getProperty("Department"));
    }
}