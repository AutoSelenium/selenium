package utilCode;

public class ReadWriteXlsxFile2 {
    public static void main(String args[]) {

        Xls_Reader datatable = new Xls_Reader("D:\\Java_Workspace\\Selenium\\src\\test\\java\\DataDrivenFiles\\RW_XlsxFile2.xlsx");

        //Reading
        int r = datatable.getRowCount("Sheet1");
        System.out.println("Number of Rows:" +r);

        int c = datatable.getColumnCount("Sheet1");
        System.out.println("Number of Columns:" +c);

        String data = datatable.getCellData("Sheet1", "City", 2);
        System.out.println(data);

        data = datatable.getCellData("Sheet1", "Name", 3);
        System.out.println(data);

        datatable.addColumn("Sheet1", "State");
        System.out.println("Column Added");

        datatable.removeColumn("Sheet1", 3, "State");
        System.out.println("Column Removed");

        datatable.addSheet("NewSheet");
        System.out.println("Sheet Added");

        datatable.removeSheet("NewSheet");
        System.out.println("Sheet Removed");

        Boolean s = datatable.isSheetExist("Sheet1");
        System.out.println(s);

        //Writing
        datatable.setCellData("Sheet1", "Name", 6 , "Hello");

    }
}
